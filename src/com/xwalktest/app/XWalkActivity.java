package com.xwalktest.app;

import org.xwalk.core.XWalkPreferences;
import org.xwalk.core.XWalkResourceClient;
import org.xwalk.core.XWalkUIClient;
import org.xwalk.core.XWalkView;

import com.xwalktest.app.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class XWalkActivity extends Activity {

	private XWalkView mXWalkView;
	
	class MyResourceClient extends XWalkResourceClient {
        MyResourceClient(XWalkView view) {
            super(view);
        }
    }

    class MyUIClient extends XWalkUIClient {
        MyUIClient(XWalkView view) {
            super(view);
        }
    }

	  @Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_xwalk);
	    mXWalkView = (XWalkView) findViewById(R.id.activity_main);
	    mXWalkView.setResourceClient(new MyResourceClient(mXWalkView));
        mXWalkView.setUIClient(new MyUIClient(mXWalkView));
        
	    // turn on debugging
	    XWalkPreferences.setValue(XWalkPreferences.REMOTE_DEBUGGING, true);
	    
	    mXWalkView.addJavascriptInterface(new JavaScriptInterface(this), "JSInterface");
	    mXWalkView.load("http://map.dreamview-staging.com", null);
	    mXWalkView.load("http://map.dreamview-staging.com", null);
		
	  }
	  
	  @Override
	  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	      if (mXWalkView != null) {
	          mXWalkView.onActivityResult(requestCode, resultCode, data);
	      }
	  }
	
	  @Override
	  protected void onNewIntent(Intent intent) {
	      if (mXWalkView != null) {
	          mXWalkView.onNewIntent(intent);
	      }
	  }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.xwalk, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public class JavaScriptInterface {
		private final Activity activity;
		private final String THIS_FILE = "JSINTERFACE";

		public JavaScriptInterface(@NonNull Activity activity) {
			this.activity = activity;
		}

		@JavascriptInterface
		public void showToast(String toast) {
			Toast.makeText(activity.getApplicationContext(), toast, Toast.LENGTH_SHORT).show();
		}

		@JavascriptInterface
		@Override
		public String toString() {
			return THIS_FILE;
		};

		@JavascriptInterface
		public void callOne(String contact) {
			Toast.makeText(activity.getApplicationContext(), "callOne() has been called", Toast.LENGTH_SHORT).show();
		}

		@JavascriptInterface
		public void callGroup(int groupId) {
			Toast.makeText(activity.getApplicationContext(), "callGroup() has been called", Toast.LENGTH_SHORT).show();
		}

		@JavascriptInterface
		public void messageOne(String contact) {
			Toast.makeText(activity.getApplicationContext(), "messageOne() has been called", Toast.LENGTH_SHORT).show();
		}

		@JavascriptInterface
		public void geoMessage(String geoLoc) {
			Toast.makeText(activity.getApplicationContext(), "geoMessage() has been called", Toast.LENGTH_SHORT).show();
		}

		@JavascriptInterface
		public void instantMessageMultiple(String contacts, String message) {
			Toast.makeText(activity.getApplicationContext(), "instantMessageMultiple() has been called", Toast.LENGTH_SHORT).show();
		}

		@JavascriptInterface
		public void startPTTOnGroupChannel(int groupId) {
			Toast.makeText(activity.getApplicationContext(), "startPTTOnGroupChannel() has been called", Toast.LENGTH_SHORT).show();
		}

		@JavascriptInterface
		public void startPTTOnUserList(String contacts) {
			Toast.makeText(activity.getApplicationContext(), "startPTTOnUserList() has been called", Toast.LENGTH_SHORT).show();
		}

		@JavascriptInterface
		public void pickImage(final String addressLeftovers, final String callback) {
			Toast.makeText(activity.getApplicationContext(), "pickImage() has been called", Toast.LENGTH_SHORT).show();
		}
	}
}
